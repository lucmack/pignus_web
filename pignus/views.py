from django.shortcuts import render

# Create your views here.
def home(request):
    return render(request,'home.html')

def somos(request):
    return render(request,'somos.html')

def contactenos(request):
    return render(request,'contactenos.html')    
    
def registro(request):
    return render(request,'registro.html')      