
from django.contrib import admin
from django.urls import path,include
from .views import home, somos,contactenos,registro

from . import views

urlpatterns = [
  path('',home,name="home"),
  path('somos/',somos,name="somos"),
  path('contactenos/',contactenos,name="contactenos"),
  path('registro/',registro,name="registro"),
 
]